import { Accordion, AccordionPanel, Box, Grid, Image, Text } from 'grommet'
import React from 'react'

export const Profil = () => {
        return(
            <Box fill>
                <Grid
                fill
                rows={['1/1']}
                columns={['1/3','2/3']}
                gap="none"
                areas={[
                        { name: 'left', start: [0, 0], end: [0, 0] },
                        { name: 'right', start: [1, 0], end: [1, 0] },
                    ]}
                >
                    <Box gridArea="left" fill justify="center" align="center">
                        <Text style={{fontFamily: 'monospace', fontSize: 38}}>
                            Parcours scolaire
                        </Text>
                    </Box>
                    <Box gridArea="right" fill background="bg">
                    <Accordion multiple>
                        <AccordionPanel label="Lycée - Bac STI2D Option SIN">
                            <Box pad="medium" background="white">
                                <Text style={{fontSize: 22, color: "#000"}} weight="bold">• Bac STI2D - Angers - Mention très bien</Text>
                            </Box>
                        </AccordionPanel>
                        <AccordionPanel label="DUT Multimédia et Métiers de l'Internet">
                            <Box pad="medium" background="white">
                                <Text weight="bold" style={{fontSize: 22 , color: "#000"}} >• Dut 1 - Laval</Text>
                            </Box>
                            <Box pad="medium" background="white">
                                <Text weight="bold" style={{fontSize: 22, color: "#000"}} >• Dut 2 - Laval (Alternance)</Text>
                            </Box>
                        </AccordionPanel>
                        <AccordionPanel label="Licence Professionnelle DReAM">
                            <Box pad="medium" background="white">
                                <Text weight="bold" style={{fontSize: 22, color: "#000"}} >• LP - Castres (Alternance)</Text>
                            </Box>
                        </AccordionPanel>
                        <AccordionPanel label="Mastère YNOV">
                            <Box pad="medium" background="white">
                                <Text weight="bold" style={{fontSize: 22, color: "#000"}} >• Mastère 1 Dev' Mobile et IoT - Toulouse (Alternance)</Text>
                            </Box>
                        </AccordionPanel>
                    </Accordion>
                    </Box>
                </Grid>
            </Box>
        )
}