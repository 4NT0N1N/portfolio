import { Accordion, AccordionPanel, Box, Grid, Image, Text } from 'grommet'
import { FormPrevious, FormNext } from 'grommet-icons';
import React from 'react'
import { path } from '../../constantes/theme';

export default class Hobbies extends React.Component {
    constructor(props)
    {
        super(props)
        this.state = {
            index: 0
        }
        
        this.data = [
            {
                content: "Je joue au jeux vidéos depuis l'âge de 4 ans, en commencant par la mythique Gamecube de nintendo en ayant des jeux comme Stawr wars, Need for speed.. Puis je suis passé sur les consoles portables comme par exemple la gameboy advance avec des jeux pokémons.. Mais là où j'ai passé le plus de temps c'est bien sûr sur mon PC ! Avec WoW ou League of legends et en passant même par Dofus.", 
                title: "Jeu-vidéo", 
                image: require("../../images/Game.jpg")
            },
            {
                content: "Passionné de foot je supporte le même club depuis tout petit, Angers SCO, le club de la ville où je suis né. J'adore aller voir les matchs directement dans les tribunes mais la situation actuelle ne nous le permet plus. Heureusement pour moi il est toujours possible de suivre et supporter mon équipe sur internet !", 
                title: "Football", 
                image: require("../../images/Football.jpg")
            },
            {
                content: "L'aviation est un vaste monde, et ce que je préfère là dedans c'est la voltige aérienne, je trouve ce sport réellement impressionnant. Que ce soit des loopings ou des vrilles toutes les figures executées sont à mon sens folles. J'ai eu l'occasion de m'y essayer et la sensation que cela procure est dingue, je vous le conseille vivement.", 
                title: "Aviation", 
                image: require("../../images/Airplane.jpg")}
        ]
    }

    previous = () => {
        const {index} = this.state
        if(index - 1 !== -1)
        {
            this.setState({index: index - 1})
        }
        else
        {
            this.setState({index: 2})
        }
    } 

    next = () => {
        const {index} = this.state
        if(index + 1 !== this.data.length)
        {
            this.setState({index: index + 1})
        }
        else
        {
            this.setState({index: 0})
        }
    } 

    render()
    {
        const {index} = this.state;
        return(
            <Box fill>
                <Grid
                fill
                rows={['1/2', '1/2']}
                columns={['1/1']}
                gap="none"
                areas={[
                        { name: 'up', start: [0, 0], end: [0, 0] },
                        { name: 'down', start: [0, 1], end: [0, 1] },
                    ]}
                >
                    <Box gridArea="up" fill justify="center" align="center">
                        <Grid
                        fill
                        rows={['1/1']}
                        columns={['1/4', '2/4', '1/4']}
                        gap="none"
                        areas={[
                                { name: 'left', start: [0, 0], end: [0, 0] },
                                { name: 'center', start: [1, 0], end: [1, 0] },
                                { name: 'right', start: [2, 0], end: [2, 0] },
                            ]}
                        >
                            <Box justify="center" align="center" gridArea="left"> 
                                <FormPrevious color="#000"  size="xlarge" onClick={() => this.previous()} />
                            </Box>
                            <Box justify="center" fill align="center" justify="center" gridArea="center">
                                <Box style={{borderColor: "#000", borderWidth: 5, border: "solid", height: "90%", width: "90%"}}>
                                    <Image src={this.data[index].image} fit="cover" />
                                </Box>
                            </Box>
                            <Box justify="center" align="center" gridArea="right">
                                <FormNext color="#000" size="xlarge" onClick={() => this.next()} />
                            </Box>
                        </Grid>
                    </Box>
                    <Box gridArea="down" fill background="bg">
                        <Grid
                            fill
                            rows={['1/4', '3/4']}
                            columns={['1/1']}
                            gap="none"
                            areas={[
                                    { name: 'up', start: [0, 0], end: [0, 0] },
                                    { name: 'down', start: [0, 1], end: [0, 1] },
                                ]}
                            >
                                <Box gridArea="up" fill justify="center" align="center">
                                    <Text weight="bold" textAlign="center" style={{paddingTop: 10, paddingBottom: 10, fontSize: 26, textDecoration: "underline"}}>{this.data[index].title}</Text>
                                </Box>
                                
                                <Box fill style={{padding: "5%"}} gridArea="down" justify="center" align="center">
                                    <Text style={{fontFamily: 'monospace', textAlign: 'justify', fontSize: 16}}>{this.data[index].content}</Text>
                                </Box>
                            </Grid>
                        </Box>
                    </Grid>
            </Box>
        )
    }
}