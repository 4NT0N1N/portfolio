import React from 'react'
import { Home as WelcomeIcon, Analytics, Article, Bar, DocumentConfig, Code } from 'grommet-icons';
import { Nav, Anchor, Box } from 'grommet'

import { 
    Link
  } from 'react-router-dom';

export default class Navigator extends React.Component {
    constructor(props)
    {
        super(props)
        this.state = {}
    }

    render()
    {
        return(
            <Nav direction="row-responsive" background="bg" pad="large">
                  <Link to="/">
                      <Anchor icon={<WelcomeIcon />} hoverIndicator />
                  </Link>
                  <Link to="/home">
                    <Anchor icon={<Article />} hoverIndicator />
                  </Link>
                  <Link to="/profil">
                    <Anchor icon={<Analytics />} hoverIndicator />
                  </Link>
                  <Link to="/hobbies">
                    <Anchor icon={<Bar />} hoverIndicator />
                  </Link>
                  <Link to="/skills">
                    <Anchor icon={<DocumentConfig />} hoverIndicator />
                  </Link>
                  <Link to="/internship">
                    <Anchor icon={<Code />} hoverIndicator />
                  </Link>
                  
            </Nav>
        )
    }
}