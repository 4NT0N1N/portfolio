import { Box, Grid, Image, Text, Meter } from 'grommet'
import React from 'react'

// Peut être optimisé avec un tableau d'objet JSON et un map de celui-ci

export const Internship = () => {
    return(
        <Box fill>
            <Grid
            fill
            rows={['1/2', '1/2']}
            columns={['1/2','1/2']}
            gap="none"
            areas={[
                    { name: 'left', start: [0, 0], end: [0, 1] },
                    { name: 'up-right', start: [1, 0], end: [1, 0] },
                    { name: 'down-right', start: [1, 1], end: [1, 1] },
                ]}
            >
                 <Box gridArea="left" fill justify="center" align="center">
                    <Box height="35vw" width="35vw">
                        <Image src={require("../../images/phf.svg")} fit="contain" />
                    </Box>
                </Box>
                <Box gridArea="up-right" fill>
                    <Grid
                    fill
                    rows={['xxsmall', 'xxsmall', 'xxsmall']}
                    columns={['full']}
                    gap={{row: "xlarge", column: "none"}}
                    areas={[
                        { name: 'name', start: [0, 0], end: [0, 0] },
                        { name: 'familyName', start: [0, 1], end: [0, 1] },
                        { name: 'title', start: [0, 2], end: [0, 2] },
                    ]}
                    >
                        <Text gridArea="name"  size="5vw" weight="bold" color="orange" style={{fontFamily: 'monospace'}}>Association</Text>
                        <Text gridArea="familyName" size="6vw" color="phf" weight="bold" style={{fontFamily: 'monospace'}}>Prim'Holstein</Text>
                        <Text gridArea="title" size="3vw" style={{fontFamily: 'monospace'}}>France</Text>
                    </Grid>
                </Box>
                <Box gridArea="down-right" background="bg" fill style={{paddingLeft: "5%"}} justify="center">
                    <Grid
                    rows={['xxsmall', 'xxsmall', 'xxsmall', 'xxsmall']}
                    columns={['full']}
                    gap={{row: "xxsmall", column: "none"}}
                    areas={[
                        { name: 'first', start: [0, 0], end: [0, 0] },
                        { name: 'second', start: [0, 1], end: [0, 1] },
                        { name: 'third', start: [0, 2], end: [0, 2] },
                        { name: 'fourth', start: [0, 3], end: [0, 3] },
                    ]}
                    >
                        <Text gridArea="first">Développeur react-native et C# sur des projets différents</Text>
                        <Text gridArea="second">Alternance à PHF depuis plus de 2 ans</Text>
                        <Text gridArea="third">2 applications disponibles sur les stores</Text>
                        <Text gridArea="fourth" textAlign="center">
                            <a href="https://play.google.com/store/apps/details?id=com.tester_accouplement&hl=fr&gl=US">
                                Illicow
                            </a>
                            ||
                            <a href="https://play.google.com/store/apps/details?id=com.phf.guidetaureaux&hl=fr&gl=US">
                                Guide Taureaux
                            </a>
                        </Text>
                    </Grid>
                </Box>
            </Grid>
        </Box>
    )
}