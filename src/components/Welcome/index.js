import { Box } from 'grommet'
import React from 'react'
import { useHistory } from 'react-router-dom'
import Scramble from 'react-scramble'
export const Welcome = () => {
    const history = useHistory();
    setTimeout(() => {
        history.push('/home');
    }, 5000)
        return(
                <Box fill justify="center" align="center" background="bg">
                    <Scramble
                        autoStart
                        text="Welcome !"
                        steps={[
                        {
                            roll: 30,
                            action: '+',
                            type: 'all',
                        },
                        {
                            action: '-',
                            type: 'forward',
                        },
                        ]}
                    />
                </Box>
        )
}