import React from "react";
import { Switch, Route, withRouter  } from "react-router-dom";
import { Box } from "grommet";
import { TransitionGroup, CSSTransition } from "react-transition-group";

/** Pages **/
import { Welcome } from '../Welcome';
import { Home } from "../Home";
import { Profil } from "../Profil";
import Hobbies from "../Hobbies";

/** Style **/
import "./styles.css";
import { Skills } from "../Skills";
import { Internship } from "../Internship";



class Container extends React.Component {
    render()
    {
        const {location} = this.props
        return (
            <Box fill>
                <TransitionGroup className="transition-group">
                    <CSSTransition
                    key={location.key}
                    timeout={{ enter: 300, exit: 300 }}
                    classNames="fade"
                    >
                        <section className="route-section">
                            <Switch location={location}>
                                <Route exact path="/" component={Welcome} />
                                <Route path="/home" component={Home} />
                                <Route path="/profil" component={Profil} />
                                <Route path="/hobbies" component={Hobbies} />
                                <Route path="/skills" component={Skills} />
                                <Route path="/internship" component={Internship} />
                            </Switch>
                        </section>
                    </CSSTransition>
                </TransitionGroup>
            </Box>
          );
    }
}


export default withRouter(Container)
