import { Box, Grid, Image, Text, Meter } from 'grommet'
import React from 'react'

// Peut être optimisé avec un tableau d'objet JSON et un map de celui-ci

export const Skills = () => {
        return(
            <Box fill>
                <Grid
                fill
                columns={['full']}
                rows={['small', 'xsmall', 'xsmall', 'xsmall', 'xsmall', 'xsmall', 'xsmall',]}
                gap="none"
                areas={[
                        { name: 'title', start: [0, 0], end: [0, 0] },
                        { name: 'first', start: [0, 1], end: [0, 1] },
                        { name: 'second', start: [0, 2], end: [0, 2] },
                        { name: 'third', start: [0, 3], end: [0, 3] },
                        { name: 'fourth', start: [0, 4], end: [0, 4] },
                        { name: 'fifth', start: [0, 5], end: [0, 5] },
                        { name: 'sixth', start: [0, 6], end: [0, 6] },
                    ]}
                >
                    <Box gridArea="title" background="bg" direction="row" fill justify="center" align="center">
                        <Text size="xxlarge">Mes skills</Text>
                    </Box>
                    <Box gridArea="first" direction="row" fill justify="around" align="center">
                        <Box justify="center" align="center">
                            <Image src={require("../../images/html.svg")} fit="contain" margin="small" width={60} />
                        </Box>
                        <Meter round background="bg" values={[{ value: 80, label: 'HTML/CSS', onClick: () => {}}]} />
                        <Text style={{width: 80}}>HTML / CSS</Text>
                    </Box>
                    <Box gridArea="second" background="bg" direction="row" fill justify="around" align="center">
                        <Box justify="center" align="center">
                            <Image src={require("../../images/js.svg")} fit="contain" margin="small" width={60} />
                        </Box>
                        <Meter round background="white" values={[{ value: 86, label: 'Javascript', onClick: () => {}}]} />
                        <Text style={{width: 80}}>Javascript</Text>
                    </Box>
                    <Box gridArea="third" direction="row" fill justify="around" align="center">
                        <Box justify="center" align="center">
                            <Image src={require("../../images/android.svg")} fit="contain" margin="small" width={60} />
                        </Box>
                        <Meter round background="bg" values={[{ value: 74, label: 'Android', onClick: () => {}}]} />
                        <Text style={{width: 80}}>Android</Text>
                    </Box>
                    <Box gridArea="fourth" background="bg" direction="row" fill justify="around" align="center">
                        <Box justify="center" align="center">
                            <Image src={require("../../images/react.svg")} fit="contain" margin="small" width={60} />
                        </Box>
                        <Meter round background="white" values={[{ value: 90, label: 'react-native', onClick: () => {}}]} />
                        <Text style={{width: 80}}>react-native</Text>
                    </Box>
                    <Box gridArea="fifth" direction="row" fill justify="around" align="center">
                        <Box justify="center" align="center">
                            <Image src={require("../../images/xd.svg")} fit="contain" margin="small" width={60} />
                        </Box>
                        <Meter round background="bg" values={[{ value: 80, label: 'Adobe XD', onClick: () => {}}]} />
                        <Text style={{width: 80}}>Adobe XD</Text>
                    </Box>
                    <Box gridArea="sixth" background="bg" direction="row" fill justify="around" align="center">
                        <Box justify="center" align="center">
                            <Image src={require("../../images/premier.svg")} fit="contain" margin="small" width={60} />
                        </Box>
                        <Meter round background="white" values={[{ value: 80, label: 'Adobe Premiere', onClick: () => {}}]} />
                        <Text style={{width: 80}}>Adobe Premiere</Text>
                    </Box>
                </Grid>
            </Box>
        )
}