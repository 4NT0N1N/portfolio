import { Box, Grid, Image, Text } from 'grommet'
import React from 'react'

export const Home = () => {
        return(
            <Box fill>
                <Grid
                fill
                rows={['1/2', '1/2']}
                columns={['1/2','1/2']}
                gap="none"
                areas={[
                        { name: 'up-left', start: [0, 0], end: [0, 0] },
                        { name: 'down-left', start: [0, 1], end: [0, 1] },
                        { name: 'right', start: [1, 0], end: [1, 1] },
                    ]}
                >
                    <Box gridArea="up-left" fill background="bg" style={{paddingLeft: "10%"}}>
                        <Grid
                        fill
                        rows={['xxsmall', 'xxsmall', 'xxsmall']}
                        columns={['large']}
                        gap={{row: "xlarge", column: "none"}}
                        areas={[
                            { name: 'name', start: [0, 0], end: [0, 0] },
                            { name: 'familyName', start: [0, 1], end: [0, 1] },
                            { name: 'title', start: [0, 2], end: [0, 2] },
                        ]}
                        >
                            <Text gridArea="name" size="5vw" weight="bold" style={{fontFamily: 'monospace', paddingLeft: 10}}>Antonin </Text>
                            <Text gridArea="familyName" size="7vw" weight="bold" style={{fontFamily: 'monospace'}}>Le Nevez </Text>
                            <Text gridArea="title" size="3vw" style={{fontFamily: 'monospace', paddingLeft: 10}}>React Lover</Text>
                        </Grid>
                    </Box>
                    <Box gridArea="down-left" fill style={{paddingLeft: "10%"}} justify="center">
                        <Grid
                        rows={['xxsmall', 'xxsmall', 'xxsmall']}
                        columns={['large']}
                        gap={{row: "xxsmall", column: "none"}}
                        areas={[
                            { name: 'first', start: [0, 0], end: [0, 0] },
                            { name: 'second', start: [0, 1], end: [0, 1] },
                            { name: 'third', start: [0, 2], end: [0, 2] },
                        ]}
                        >
                            <Text gridArea="first">Excellent ability to work in cooperation</Text>
                            <Text gridArea="second">Experience at talking to large audiences</Text>
                            <Text gridArea="third">Good leadership qualities as a head of a group</Text>
                        </Grid>
                    </Box>
                    <Box gridArea="right" fill justify="center" align="center">
                        <Box background="bg" height="35vw" width="35vw" border={{size: "5px", color: "#000"}}>
                            <Image src={require("../../images/Profil.jpg")} fit="contain" />
                        </Box>
                    </Box>
                </Grid>
            </Box>
        )
}