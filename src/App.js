import React from 'react';
import logo from './logo.svg';
import './App.css';
import { Box, Grommet } from 'grommet';
import Home from './components/Home';
import Welcome from './components/Welcome';
import Navigator from './components/Navigator';
import { theme, welcome } from './constantes/theme';

import { 
  BrowserRouter as Router, 
  Redirect,
} from 'react-router-dom';
import Container from './components/Container';

class App extends React.Component {

  render()
  {
    return (
      <Router>
        <Grommet theme={theme} full>
          <Container />
          <Navigator />
        </Grommet>
      </Router>
    );
  }
}

export default App;
