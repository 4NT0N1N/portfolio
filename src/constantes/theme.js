export const theme = {
    global: { 
        colors: { 
            bg: '#000',
            white: '#fff',
            phf: "#013e7e",
            orange: "#de7022",
        },
        font:  {
            family: 'letter',
            size: '18px',
        },
    } 
}

export const welcome = {
    global: { 
        colors: { 
            bg: '#000',
            white: '#fff',
            phf: "#013e7e",
        },
        font:  {
            family: 'letter',
            size: '5vw',
        },
    } 
}

export const path = "..\src\images\Game.jpg"
